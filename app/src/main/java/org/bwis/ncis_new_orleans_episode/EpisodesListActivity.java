package org.bwis.ncis_new_orleans_episode;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

/**
 * Created by Benayha on 29-3-2018.
 */

public class EpisodesListActivity extends AppCompatActivity {

    public int EpisodesListStatus = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episodes_list);

        setEpisodesListView(EpisodesListStatus);

    }

    /**
     * Set the EpisodeListView items from the SerieSeasonList obrject or SerieEpisodeList
     * if EpisodesListStatus = 0 use SerieSeasonList
     * if EpisodesListStatus = 1 use SerieEpisodeList
     * @param EpisodesListSt
     */
    public void setEpisodesListView(int EpisodesListSt){

    }

    /**
     * changes EpisodesListStatus to the needed list
     * 0 for Season list
     * 1 for Episode list
     * @param EpStatus
     */
    public void setEpisodesListStatus(int EpStatus){
        EpisodesListStatus = EpStatus;
    }

    public void setSerieInfoTitle(){

    }

}
