package org.bwis.ncis_new_orleans_episode.objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by benay on 27-3-2018.
 */

public class SerieSeasonList {

    public List<String> seasonList = new ArrayList<String>();

    /**
     * Construct the SerieSeasonListObject from where the listview can be populated
     * with the amount of seasons the Serie is running.
     */
    public void serieSeasonList(){

        for(int i = 1; i <= getAmountOfSeasons(); i++) {
            String inputString = "Season " + i;
            seasonList.add(inputString);
        }


    }

    /**
     * Retrieving the amount of seasons of the serie from the ContentProvider
     */
    private int getAmountOfSeasons(){

        return 2;
    }
}
