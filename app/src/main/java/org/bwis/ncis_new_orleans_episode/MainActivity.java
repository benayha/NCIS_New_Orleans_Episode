package org.bwis.ncis_new_orleans_episode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity{

    public int appStatus = 0 ;
    private Button getSeasonsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /**
         * Trigger de Databaser service to get the latest info
         */
        startGetInfoFromServer();

    }

    /**
     * Method for starting theservice to get the latest info from server.
     */
    public void startGetInfoFromServer(){

    }

    /**
     * set the episodes button depending on de stutus of the app
     * if appStatus = 0 there are no episodes in Database so button is not active
     * if appStatus = 1 there are episodes in Dataabase so button is active
     */
    public void setEpisodesbutton(){

    }

    /**
     * set the serieInfoTitle depending on de status of the app
     * if appStatus = 0 set serieInfoTitle to: Retrieving serie info
     * if AppStatus = 1 get the serieInfoTitle from SerieMainInfoObject
     */
    public void setSerieInfoTitle(){

    }

    /**
     * set the serieInfo depending on the status of the app
     * if appStatus = 0 set serieInfo empty
     * if appStatus = 1 set serie get the serInfo form serieMainInfoObject
     */
    public void setSerieInfo(){

    }
}

